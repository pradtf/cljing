(ns cljing.core
  (:require [net.cgrand.enlive-html :as html]
            [tech.ml.dataset :as ds]
            [tech.v2.datatype :as dtype]
            [tech.v2.datatype.functional :as dfn]))

;; * practicalli guide
;; ;; define the template
;; (html/deftemplate post-page "post.html"
;;   [post]
;;   [:title] (html/content (:title post))
;;   [:h1] (html/content (:title post))
;;   [:spanclass.author] (html/content (:author post))
;;   [:div.post-body] (html/content (:body post)))


;; (def sample-post {:author "Luke VanderHart"
;;                   :title "Why Clojure Rocks"
;;                   :body "Functional programming!"})

;; (reduce str (post-page sample-post))

https://practicalli.github.io/blog/posts/web-scraping-with-clojure-hacking-hacker-news/

(def hacker-news-url "https://news.ycombinator.com/")

(def website-content-hacker-news
  "get website content from hacker news
  returns: list of html content as hash-maps"
  (html/html-resource (java.net.URL. hacker-news-url)))

(take 1 (html/select website-content-hacker-news [:td.title :a]))
;; => ({:tag :a, :attrs {:href "https://www.nngroup.com/articles/anti-mac-interface/", :class "storylink"}, :content ("The Anti-Mac User Interface (1996)")})

(take 1 (map html/text
             (html/select
              website-content-hacker-news
              [:td.title :a])))
;; => ("The Anti-Mac User Interface (1996)")

;; (pprint website-content-hacker-news (clojure.java.io/writer "test.txt"))


;; * enlive-tutorial
;; https://github.com/swannodette/enlive-tutorial

;; scrape1
(def ^:dynamic *base-url* "https://news.ycombinator.com/")

(defn fetch-url [url]
  (html/html-resource (java.net.URL. url)))

(def data (fetch-url "https://news.ycombinator.com/"))

(defn hn-headlines []
  (map html/text (html/select data [:td.title :a])))

(defn hn-points []
  (map html/text (html/select data [:td.subtext html/first-child])))

(defn print-headlines-and-points1 []
  (doseq [line (map #(str %1 " (" %2 ")") (hn-headlines) (hn-points))]
    (println line)))

(hn-headlines)
(hn-points)
(print-headlines-and-points1)


;; scrape2
(defn hn-headlines-and-points []
  (map html/text
       (html/select data
                    #{[:td.title :a] [:td.subtext html/first-child]})))

(defn print-headlines-and-points2 []
  (doseq [line (map (fn [[h s]] (str h " (" s ")"))
                    (partition 2 (hn-headlines-and-points)))]
    (println line)))

(hn-headlines-and-points)
(print-headlines-and-points2)


;; scrape3
(def ^:dynamic *base-url* "http://nytimes.com/")







;; * stock analysis
;; <p class="Mt(15px) Lh(1.6)" data-reactid="217">

;; exchanges info
"
2281 AMEX.txt
Symbol	Description
AAA	First Priority Clo Bond ETF
AAAU	Goldman Sachs Physical Gold ETF
AADR	Advisorshares Dorsey Wright ETF
AAMC	Altisource Asset
AAU	Almaden Minerals
ABEQ	Absolute Core Strategy ETF
ACES	Alps Clean Energy ETF
ACIO	Aptus Collared Income Opportunity ETF
ACSG	Xtrackers MSCI ACWI Ex USA ESG Leaders ETF

3796 ASX.txt
Symbol	Description
14D.AX	1414 Degrees Limited
1AD.AX	Adalta Limited
1ADO.AX	Adalta Limited
1AG.AX	Alterra Limited
1ST.AX	1st Available Ltd
2BE.AX	Tubi Limited
2BEO.AX	2BEO
360.AX	Life360 Inc.
3DA.AX	Amaero International Ltd

1966 HKEX.txt
Symbol	Description
00001.HK	Cheung Kong (Holdings) Ltd
00002.HK	CLP Holdings Ltd
00003.HK	Hong Kong and China Gas Co. Ltd
00004.HK	Wharf (Holdings) Ltd
00005.HK	HSBC Holdings plc
00006.HK	Power Asset Holdings
00007.HK	Hoifu Energy
00008.HK	PCCW Ltd
00009.HK	Mandarin Entertainment (Holdings) Ltd

6319 LSE.txt
Symbol	Description
02NG.L	STATOILHYDRO ASA 6.125% NOTES 27/11/28 GBP(VAR)
0A0B.L	AHLERS AG ORD NPV
0A0C.L	AHLERS AG NON VTG PREF NPV
0A1U.L	
0A2K.L	AGUAS DE SABADELL SER`A`EUR12.02
0A2L.L	AGUAS DE SABADELL SER`B`EUR12.02
0A3R.L	ALANDSBANKEN `A`NPV
0A3T.L	ALANDSBANKEN `B`NPV
0A5J.L	ANEK LINES SA EUR1.61(PREF REGD)`1990`

3929 NASDAQ.txt
Symbol	Description
AACG	Ata Creativity Global
AACQ	Artius Acquisition Inc Cl A
AACQU	Artius Acquisition Inc Unit
AACQW	Artius Acquisition Inc WT
AAL	American Airlines Gp
AAME	Atlantic Amer Cp
AAOI	Applied Optoelect
AAON	Aaon Inc
AAPL	Apple Inc

3308 NYSE.txt
Symbol	Description
A	Agilent Technologies
AA	Alcoa Corp
AAI-B	Arlington Asset Investment Corp [Aaic/Pb]
AAI-C	Arlington Asset Investment Corp [Ai/Pc]
AAIC	Arlington Asset Investment Corp
AAN	Aarons Holdings Company Inc
AAN.P	The Aarons Company Inc [Aan/I]
AAP	Advance Auto Parts Inc
AAT	American Assets Trust

1148 SGX.txt
Symbol	Description
1A0.SI	Katrina Group Ltd.
1A1.SI	Wong Fong Industries Limited
1A4.SI	Agv Group Limited
1B0.SI	Mm2 Asia Ltd.
1B1.SI	Hc Surgical Specialistslimited
1B5W.SI	Charisma Energyservicesw211128
1B6.SI	Ocean Sky International Ltd
1C0.SI	Cedar Strategic Holdings Ltd
1C3.SI	Samurai 2k Aerosol Limited

2361 TSX.txt
Symbol	Description
AAB.TO	Aberdeen International Inc
AAV.TO	Advantage Oil & Gas Ltd
ABST.TO	Absolute Software Corp
ABX.TO	Barrick Gold Corp
AC.TO	Air Canada
ACB.TO	Aurora Cannabis Inc
ACB.WT.U.TO	Aurora Cannabis Inc Wts USD
ACD.TO	Accord Financial
ACD.DB.TO	Accord Financial Corp 7.00 Pct Debs

1641 TSXV.txt
Symbol	Description
A.V	Armor Minerals Inc
AAG.V	Aftermath Silver Ltd
AAJC.P.V	Aaj Capital 2 Corp
AAN.V	Aton Resources Inc
AAP.H.V	Alpha Peak Leisure Inc
AAT.V	Ati Airtest Technologies Inc
AAX.V	Advance Gold Corp
AAZ.V	Azincourt Energy Corp
AB.H.V	Asbestos Corporation Limited
"


;; keyword lists in csv files

(def nays (csv-read "/home/pradmin/clj/cljing/resources/nay.txt"))
(def yays (csv-read "/home/pradmin/clj/cljing/resources/yay.txt"))


;; useful functions

(defn csv-read
  "reads a csv file given filename"
  [filename]
  (map s/trim
       (first (with-open
                [in-file (clojure.java.io/reader filename)]
                (doall
                 (clojure.data.csv/read-csv (clojure.java.io/reader in-file)))))))

(defn join-regex
  "joins regex strings: patterns -> str -> re-pattern"
  [& patterns]
  (re-pattern (apply str
                     (map #(str %) patterns))))


;; get the descriptions from the yahoo finance profiles of a stock
;; composing mkurl, fetch, pull-description into get-description

(defn mkurl
  "creates a url on finance.yahoo using stock symbol"
  [sym]
  (str "https://finance.yahoo.com/quote/" sym "/profile"))

(defn fetch-url
  "gets the html from the url provided"
  [url]
  (html/html-resource (java.net.URL. url)))

(defn pull-profile
  "gets the 3rd item from finance.yahoo profile of a stock"
  [txt]
  (nth (map html/text
            (html/select txt [:p]))
       2))

;; an improvement from pull-profile which tries to deal with
;; the item not being there by checking the size of the vector
;; however, it is no longer necessary since we know how to enlive
(defn get-desc
  "pulls the description of the stock from the html text"
  [txt]
  (let [ps (map html/text (html/select txt [:p]))
        p-count (count ps)]
    (when (> p-count 2)
      (nth (map html/text (html/select txt [:p])) 2))))

(def get-profile ;from page given url
  (comp pull-profile fetch-url mkurl))

(defn stock-desc
  "gets stock descriptions"
  [symb comp]
  (if (re-find #"ETF" comp)
    "ETF"
    (s/trim (get-profile symb))))

;; accumulator fn to count frequency of keywords
(defn countall
  "given desc and list of words finds total count of all occurences"
  [desc ays]
  (loop [tot 0
         ays ays]
    (if (empty? ays)
      tot
      (recur
       (+ tot (count (re-seq (re-pattern (first ays)) desc)))
       (rest ays)))))
;; (map #(countall % nays) (ds-tech "Desc"))


;; ** solution using techascent

(def stocks (-> (ds/->dataset "/home/exchanges/AMEX.txt")
                (ds/rename-columns {"Description" "Company"})))
(def smocks (ds/head stocks))

;; both of these work
(map #(stock-desc % %2) (smocks "Symbol") (smocks "Company"))
(map stock-desc (smocks "Symbol") (smocks "Company"))
;; because the function is provided with its proper inputs
;; there is no need to create a literal function here


(defn final-ds
  "create the complete ds with Desc yay nay columns"
  [ds]
  (let [ds1 (assoc ds "Desc"
                      (map stock-desc (ds "Symbol") (ds "Company")))
        ds2 (assoc ds1 "yay"
                      (map #(countall % yays) (ds1 "Desc")))
        ds3 (assoc ds2 "nay"
                   (map #(countall % nays) (ds2 "Desc")))
        ds4 (assoc ds3 "may"
                   (dfn/- (ds-fin "yay") (ds-fin "nay")))]
    ds4))
(def ds-fin ;final dataset
  (final-ds smocks))
(def ds-fil ;filtered dataset by neg may
  (ds/filter-column neg? "may" ds-fin))
(ds/drop-columns (ds/filter-column neg? "may" ds-fin) ["Desc"])
;;so we just require a pos? version of this
;;also print it to tsv file with col order Symbol Company may yay nay Desc.
;;then any spreadsheet can display it.

;; ** solution using clojure data structures

(def equs ;5 equities after dropping the heading
  (drop 1
        (take 6
         (with-open
           [in-file (clojure.java.io/reader "/home/exchanges/AMEX.txt")]
           (doall
            (clojure.data.csv/read-csv (clojure.java.io/reader in-file)))))))

(defn mkmap
  "creates map of sym com des from sym\tcom"
  [itm]
  (let [[sym com] (s/split itm #"\t")
        des (stock-desc sym com)
        yay (countall des yays)
        nay (countall des nays)]
    {:sym sym :com com :des des :yay yay :nay nay}))

(def equmap ;map of equities adding descriptions
  (map #(apply mkmap %) equs))

;;todo get des seperately and then add to equmap
;; rather than doing it all in mkmap



;; ** initial attempts containing useful code
;; pullout the sentence containing desired phrase from txt (of the description)
;; todo this is somewhat poor and could use some work

(defn pull-sentence
  [phrase txt]
  (let [sentence (re-find (join-regex
                           #"[\s\w+,'\"-]+" ;accepts all \s, \w+, comma, quotes till senend
                           phrase
                           #"[\s\w+\W]+?[.!?]") ;same till senend of previous
                          txt)]
    (if sentence
      (s/triml sentence))))

(def stocks3 ["ALXN" "BYND" "SPCE"])

(def all-descriptions (map #(get-profile %) stocks3))

(def all-words ["therapeutic" "convenience store" "was founded"])

(pull-sentence "founded" txt)

(map #(pull-sentence % %2) all-words all-descriptions)

(pull-sentence "variouss"
               (nth all-descriptions 0))

;; howto cross keywords with descriptions
(for [word all-words
      desc all-descriptions]
  (pull-sentence word desc))

;; => ("develops and commercializes various therapeutic products." nil nil nil "The company sells its products through grocery, mass merchandiser, club and convenience store, natural retailer channels, direct to consumer, restaurants, foodservice outlets, and schools." nil "The company was founded in 1992 and is headquartered in Boston, Massachusetts." "was founded in 2009 and is headquartered in El Segundo, California." "was founded in 2017 and is headquartered in Las Cruces, New Mexico.")

;; todo
;; Inc. problem
;; how to present results with techascent


;; dtype/->reader required to create vector from column
(def smocks-symbols (dtype/->reader (smocks "Symbol")))

;; add column with descriptions
(assoc smocks "Desc" (map #(get-profile %) smocks-symbols))

;; add column with count of size
(assoc smocks "size" (map count (dtype/->reader (smocks "Symbol"))))

;; get vector with ETF/SYM
(assoc smocks "kind" (map etf? (smocks "Symbol") (smocks "Company")))
;; => /home/exchanges/AMEX.txt [5 3]:

| Symbol |                         Company | kind |
|--------|---------------------------------|------|
|    AAA |     First Priority Clo Bond ETF |  ETF |
|   AAAU | Goldman Sachs Physical Gold ETF |  ETF |
|   AADR | Advisorshares Dorsey Wright ETF |  ETF |
|   AAMC |                Altisource Asset | AAMC |
|    AAU |                Almaden Minerals |  AAU |


;; todo howto negative regex

